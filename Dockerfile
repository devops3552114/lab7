FROM alpine:latest as system
RUN apk update; apk upgrade; apk add maven; apk add openjdk8

FROM system as builder

WORKDIR /bezkoder-app
COPY ./pom.xml pom.xml
COPY ./src src/

RUN mvn clean package

FROM openjdk:8-jre-alpine

COPY --from=builder bezkoder-app/target/spring-boot-data-jpa-0.0.1-SNAPSHOT.jar spring-boot-data-jpa-0.0.1-SNAPSHOT.jar

CMD ["java", "-jar", "spring-boot-data-jpa-0.0.1-SNAPSHOT.jar"]